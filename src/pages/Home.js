import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home () {

	const data = {
		title: "Quencher",
		content: "Running out of drinks?",
		destination: "/courses",
		label: "Stock up"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>

		)
}