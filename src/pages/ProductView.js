import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const addToCart = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have successfully added this product to cart"
				})

				navigate("/products")
			}

			else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}

		})

	}

	useEffect(() => {
		
		// console.log(productId)
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [productId])

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span:6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>PHP {price}</Card.Text>
							{
								(user.id !== null) ?
									<Button variant="primary" onClick={() => addToCart(productId)}>Add to cart</Button>
									:
									<Link className="btn btn-danger" to="/login">Login to add this product to cart</Link>
							}
							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}