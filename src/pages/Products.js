import PropTypes from 'prop-types';
import {Fragment, useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';

export default function Products () {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/products`)
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setProducts(data.map(product => {
				return(

					<ProductCard key={product._id} productProp={product} />

					)

			}))

		})


	}, [])

	return (

			<Fragment>
				{products}
			</Fragment>
		)
}

ProductCard.propTypes = {

	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}