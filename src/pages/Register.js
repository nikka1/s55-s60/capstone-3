import {useState, useEffect, useContext} from 'react';
import {useNavigate, Navigate} from "react-router-dom";
import Swal from 'sweetalert2';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Register () {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault()


		fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please try another email address"
				})
				
			}

			else {

			// ACTION: register user

				fetch('http://localhost:4000/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {
						Swal.fire({
							title: "Welcome to Quencher",
							icon: "success",
							text: "You may now login"
						})

						navigate("/login")

					}

					else {

						Swal.fire({
							title: "Registration failed",
							icon: "error",
							text: "Please try again"
						})
					}

				})

			}

		})

	}


	useEffect(() => {

		if((email !== '' && password1 !== '' && password2 !== '')
			&&  (password1 === password2)) {
			setIsActive(true);
		}
		else {
			setIsActive(false)
		}
	}, [email, password1, password2])


	return(

		(user.id !== null) ? 
	    
	    <Navigate to ="/courses" />
	    
	    :

		<Form className="mt-3 "onSubmit = {(e) => registerUser(e)}>
		<h1>Registration page</h1>

		  <Form.Group className="mb-3" controlId="userEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control
		    				type="email"
		    				placeholder="Enter email"
		    				value = {email}
		    				onChange = {e => setEmail(e.target.value)}
		    				required />
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>


		  <Form.Group className="mb-3" controlId="password1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    				type="password" 
		    				placeholder="Password"
		    				value={password1}
		    				onChange= {e => setPassword1(e.target.value)}
		    				required />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password2">
		    <Form.Label>Verify password</Form.Label>
		    <Form.Control 
		    				type="password" 
		    				placeholder="Verify password"
		    				value={password2}
		    				onChange={e => setPassword2(e.target.value)}
		    				required />
		  </Form.Group>

		  {
		  	isActive ?
		  		<Button variant="primary" type="submit" id="submitBtn">
		    		Submit
		  		</Button>
		  		:
		  		<Button variant="danger" type="submit" id="submitBtn" disabled>
		    		Submit
		  		</Button>
		  }
		</Form>
		)
}